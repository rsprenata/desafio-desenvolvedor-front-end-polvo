$("#form-sign-up").validate({
    onkeyup: function(element) {$(element).valid()},
    rules: {
        confirmPassword: {
            equalTo: "#inputPassword1"
        }
    },
    messages: {
        email: {
            email: "E-mail inválido !"
        },
        confirmPassword: {
            equalTo: "Senha e confirmação diferentes !"
        }
    }
});

jQuery.validator.addMethod("passwordValidator", function(value, element, param) {
    var valids = 3;

    $(".validation-dot").removeClass (function (index, className) {
        return (className.match (/(^|\s)dot-\S+/g) || []).join(' ');
    });

    if (!/[0-9]/.test(value)) {
        valids--;
        $("#dotNum").addClass("dot-red");
    } else {
        $("#dotNum").addClass("dot-green");
    }
    if (!/[A-Z]/.test(value)) {
        valids--;
        $("#dotUpp").addClass("dot-red");
    } else {
        $("#dotUpp").addClass("dot-green");
    }
    if (!value || value.length < 6) {
        valids--;
        $("#dotMin").addClass("dot-red");
    } else {
        $("#dotMin").addClass("dot-green");
    }

    var color;
    if (valids <= 1) {
        color = "#F06424";
    } else if (valids === 2) {
        color = "#F0B624";
    } else if (valids === 3) {
        color = "#20D498";
    }

    $(".validation-block").each(function (k, v) {
        $(v).css("background-color", "#EBEBEB");
    });

    var it = valids;
    $(".validation-block").each(function (k, v) {
        if (it !== 0) {
            $(v).css("background-color", color);
            it--;
        }
    });

    if ($("#inputPassword1").val()) {
        $("#inputPassword1").css("border-color", color);
    } else {
        $("#inputPassword1").css("border-color", "#CED4DA");
    }

    if (valids === 3) {
        return true;
    } else {
        return false;
    }
}, "");